Feature: Testing different requests on the student application

  @SMOKE
  Scenario: Check if the student application can be accessed by users
    When User sends a GET request to the list endpoint,they must get back a valid status code 200

  @Regression
  Scenario Outline: Create a new student & verify if the student is added
    When I create a new student by providing the information firstName <firstName> lastName <lastName> email <email> programme <programme> courses <courses>
    Then I verify that the student with <email> is created

    Examples:
      | firstName | lastName | email              | programme        | courses |
      | Abubakar  | Afzal    | bakrshk@domain.com | Computer Science | AI      |
      | Cristiano | Ronaldo  | cr7@domain.com     | Arts             | Design  |

  @Regression
  Scenario Outline: Update a Student & verify the user against the id and delete the user
    * I Update the student information with student detail <id> , <firstName> , <lastName> , <email> , <programme> and <courses>
    Then I verify that the student with <id> is updated
    And I Delete the student with <id>


    Examples:
      | id   | firstName | lastName | email               | programme | courses |
      | 0007 | Shk       | shk      | bakrshk@domain1.com | Marketing | Xyz     |
      | 0008 | cr        | cr2      | cr7@domain1.com     | Business  | ABC     |

