package com.student.cucumber.steps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.student.cucumber.serenity.StudentSerenitySteps;
import com.student.cucumber.serenity.StudentSerenitySteps;
import com.student.utils.TestUtils;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class StudentSteps {
	static String email=null;

	@Steps
	StudentSerenitySteps steps;

	@When("^User sends a GET request to the list endpoint,they must get back a valid status code 200$")
	public void verify_status_code_200_for_listendpoint(){
		SerenityRest.rest()
				.given()
				.when()
				.get("/list")
				.then()
				.statusCode(200);
	}

	@When("^I create a new student by providing the information firstName (.*) lastName (.*) email (.*) programme (.*) courses (.*)$")
	public void createStudent(String firstName,String lastName,String _email,String programme,String course){
		List<String> courses = new ArrayList<>();
		courses.add(course);
		email =TestUtils.getRandomValue()+ _email;

		steps.createStudent(firstName, lastName, email, programme, courses)
				.assertThat()
				.statusCode(201);

	}

	@Then("^I verify that the student with (.*) is created$")
	public void verifyStudent(String emailId){
		HashMap<String, Object> actualValue=	steps.getStudentInfoByEmailId(email);
		assertThat(actualValue, hasValue(email));
	}

	@Given("^I Update the student information with student detail (.*) , (.*) , (.*) , (.*) , (.*) and (.*)$")
	public void updateStudents(int id,String firstname , String lastName , String email, String programme , String course){
		List<String> courses = new ArrayList<>();
		courses.add(course);

		steps.updateStudent(id,firstname,lastName,email,programme,courses)
				.assertThat()
				.statusCode(200);


	}
	@Then("^I verify that the student with (.*) is updated$")
	public void verifyStudentid(int id){
		steps.getStudentById(id)
				.assertThat().statusCode(200);

	}

	@And("^I Delete the student with (.*)$")
	public void deletestudent(int id){
		steps.deleteStudent(id);

	}
}
