# Serenity Assessment task


#### Pre-Req (Do's)
    MAVEN
    JAVA 1.8
## Setting up the Environment
    https://www.tutorialspoint.com/maven/maven_environment_setup.html
##### Run the test E2E
    
    mvn clean verify serenity:aggregate

##### Run the test By tag
    
    mvn clean verify -Dtags="studentfeature:Regression,studentfeature:SMOKE" serenity:aggregate   

##### Reporting    
    Built in Serenity Reporting 
    After execution html file will be generated 
    ==> target/site/serenity/index.html





### Dependencies

    Maven must be installed. Version < 3.xx
    verify by entering the command in terminal 
    mvn -v

>>https://maven.apache.org/install.html

    Java JDK 1.8 must be installed
    verify by entering the command in terminal 
    java -version
>>https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

